__author__ = 'Anh P. Nguyen'

from sklearn.externals import joblib
import utils


class POClassifier:
    
    def __init__(self, model_path='/mnt/filter-disk1/phananh.nguyen/models/', 
                 database='da0', 
                 user='dbo', 
                 password='sentifi', 
                 host='10.0.0.220', 
                 port=5432):
        
        #connection
        self._conn = utils.get_postgres_conn(database, user, password, host, port)
        
        
        #models
        self._sub_model_desc_cvector_nn = joblib.load(model_path + 'sub_model_desc_cvector_nn')
        self._sub_model_desc_cvector_pr = joblib.load(model_path + 'sub_model_desc_cvector_pr')
        self._sub_model_desc_nb_nn = joblib.load(model_path + 'sub_model_desc_nb_nn')
        self._sub_model_desc_nb_pr = joblib.load(model_path + 'sub_model_desc_nb_pr')

        self._sub_model_name_nb = joblib.load(model_path + 'sub_model_name_nb')
        self._sub_model_name_cvector = joblib.load(model_path + 'sub_model_name_cvector')
        
        self._sub_model_entropy_lr = joblib.load(model_path + 'sub_model_entropy_lr')

        self._meta_model_lr = joblib.load(model_path + 'lr_nameprob_descnnprob_descprprob_entropy_v1')
    
    def extract(self, sns_id):
        profile = utils.get_profile_by_id(self._conn, sns_id, utils.test_query_template)
        if profile == {}:
            return None
        if 'payload' in profile:
            payload = profile['payload']
        else: 
            return None
        profile_desc = payload[u'description']
        profile_name = payload[u'name']
        profile_url = payload[u'profile_background_image_url_https']
        noun, pronoun = utils.extract_description3(profile_desc)
        name = utils.word_normalize(profile_name)
        image_entropy, image_dat = utils.get_image_entropy_from_url(self._sub_model_entropy_lr, profile_url)
        
        return {'description_raw': profile_desc, 'name_raw': profile_name, 'image_url': profile_url,
                   'noun': noun, 'pronoun': pronoun, 'name': name, 'image_entropy': image_entropy, 'image_dat': image_dat}
        
        
    def predict(self, sns_id):
        #step 1: get profile
        profile = utils.get_profile_by_id(self._conn, sns_id, utils.test_query_template)
        if profile == {}:
            return None
        if 'payload' in profile:
            payload = profile['payload']
        else: 
            return None
        
        #step 2 :feature extraction
        profile_desc = payload[u'description']
        profile_name = payload[u'name']
        profile_url = payload[u'profile_background_image_url_https']
        
        image_entropy, image_dat = utils.get_image_entropy_from_url(self._sub_model_entropy_lr, profile_url)
        desc_nn_prob, desc_pr_prob = utils.get_description_prob(profile_desc, self._sub_model_desc_nb_nn, self._sub_model_desc_nb_pr, self._sub_model_desc_cvector_nn, self._sub_model_desc_cvector_pr)
        name_prob = utils.get_name_prob(profile_name, self._sub_model_name_nb, self._sub_model_name_cvector)
        
        #step 3: predict
        X = [[name_prob, desc_nn_prob, desc_pr_prob, image_entropy[0]]]
        org_prob = self._meta_model_lr.predict_proba(X)[0][1]
        org_predict = self._meta_model_lr.predict(X)[0] 
        if org_predict == 1:
            org_class = 'O'
        elif org_predict == 0:
            org_class = 'P'
        else:
            org_class = '?'
        
        return {'sns_id': sns_id, 'image_entropy': image_entropy[0], 'description_prob_list': [desc_nn_prob, desc_pr_prob], 'name_prob': name_prob, 'confidence': org_prob, 'class': org_class}
    