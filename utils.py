import numpy
import re
import nltk
import urllib2
#import cv2
import scipy.stats

#from psycopg2 import connect
#from psycopg2.extras import RealDictCursor

#remove url
regex_f1 = r"(?:\@|https?\://)\S+"
#split word in this form heLloWORld34tesla --> he Llo WO Rld 34 tesla
regex_f2 = '(?=[A-Z][a-z])|(?<=[a-z])(?=[A-Z])|\d+'

#NN: noun, singular
#NNS: noun, plural
#NNPS: proper noun
#PRP: personal pronoun
#PRP$: possesive pronoun

#VB: verb base form
#VBD: verb past tense
#VBG: verb gerund
#VBP: verb non-3sg
#VBZ: verb 3sg
#WP: wh-pronoun
#WP$: possesive wh-
#WRB: wh-adverb

train_query_template='''select * from sns_account where sns_name='tw' and sns_id=%s limit 1;'''
test_query_template = '''SELECT * FROM twitter.tw_user WHERE user_id=%s'''

def get_postgres_conn(database_name, user_name, password, host_name, port):
    return connect(database=database_name, user=user_name, password=password, host=host_name, port=port)


def word_normalize(sname):
    try:
    	if type(sname) == type(0.0):
            return ''
    except:
	return ''

    temp = re.sub(regex_f1, '', sname).strip()
    temp = ' '.join(re.split('\W+', temp.strip())).strip()
    temp = ' '.join(temp.split('_')).strip()
    temp = ' '.join(re.sub(regex_f2, ' ',  temp).split())
    return temp


def extract_description(desc):
    return word_normalize(desc)


def extract_description2(desc):
    temp = word_normalize(desc)
    tokens = nltk.word_tokenize(temp)
    pos_tag_list = nltk.pos_tag(tokens)
    result = []
    for item in pos_tag_list:
        if item[1] in ('NNS', 'NN', 'NNP', 'NNPS', 'PRP', 'PRP$', 'VB', 'VBP', 'VBD', 'VBZ', 'WP', 'WP$', 'WRB'):
            result += [item[0]]
    return ' '.join(result).strip()


def extract_description3(desc):
    temp = word_normalize(desc)
    tokens = nltk.word_tokenize(temp)
    pos_tag_list = nltk.pos_tag(tokens)
    prp_list = []
    nn_list = []
    for item in pos_tag_list:
        if item[1] in ('NNS', 'NN', 'NNP', 'NNPS'):
            prp_list += [item[0]]
        elif item[1] in ('PRP', 'PRP$'):
            nn_list += [item[0]]
    return ' '.join(prp_list).strip(), ' '.join(nn_list).strip()

def get_profile_by_id(conn, sns_id, query_str):
    #try:
        
        with conn.cursor(cursor_factory=RealDictCursor) as cur:
            query = cur.mogrify(query_str, (sns_id,))
            try:
                cur.execute(query)
                conn.commit()
            except Exception:
                print Exception
                conn.rollback()
            
            if cur.rowcount > 0:
                return cur.fetchone()
            else:
                return {}
            
#######------------------feature extractions-----------------------#######

def get_image_entropy_from_url(f_lr, url):
    try:
        img_raw_dat = numpy.asarray(bytearray(urllib2.urlopen(url).read()), dtype=numpy.uint8)
        img_dat = cv2.imdecode(img_raw_dat, -1)
        img_dat = cv2.cvtColor(img_dat , cv2.COLOR_RGBA2BGR);

        img_dat = cv2.fastNlMeansDenoisingColored(img_dat,None,10,10,7,21)
        img_gray = cv2.cvtColor(img_dat, cv2.COLOR_BGR2GRAY)

        hist = cv2.calcHist([img_gray], [0], None, [256], [0, 256])
        
        return scipy.stats.entropy(hist, base=2), img_gray
    except:
        mid_point = -f_lr.intercept_[0] / f_lr.coef_[0][0]
        return [mid_point], []
    

def get_profile_image_entropy(conn, f_lr, sns_id, test=False):
    try:
        if test == False:
            profile = get_profile_by_id(conn, sns_id, train_query_template)
        else:
            profile = get_profile_by_id(conn, sns_id, test_query_template)
        url = profile['payload'][u'profile_image_url_https'].replace('normal', '400x400')
        
        return get_image_entropy_from_url(url)
    except Exception, e:
        print sns_id, str(e)
        return [-1], None

    
def get_description_logprob(description, f_nb_nn, f_nb_pr, cvector_nn, cvector_pr):
    desc_refined_list = extract_description3(description)
    
    cvect_nn = cvector_nn.transform([desc_refined_list[0]])
    cvect_pr = cvector_pr.transform([desc_refined_list[1]])
    
    desc_nn_logprob = f_nb_nn.predict_log_proba(cvect_nn)[0]
    desc_pr_logprob = f_nb_pr.predict_log_proba(cvect_pr)[0]
    
    return desc_nn_logprob, desc_pr_logprob 


def get_description_prob(description, f_nb_nn, f_nb_pr, cvector_nn, cvector_pr):    
    desc_refined_list = extract_description3(description)
    
    cvect_nn = cvector_nn.transform([desc_refined_list[0]])
    cvect_pr = cvector_pr.transform([desc_refined_list[1]])
    
    desc_nn_prob = f_nb_nn.predict_proba(cvect_nn)[0]
    desc_pr_prob = f_nb_pr.predict_proba(cvect_pr)[0]
    
    return desc_nn_prob[1], desc_pr_prob[1]
    
    
def get_name_logprob(name_raw, f_nb, cvector_nb):
    name = word_normalize(name_raw)
    name_logprob = f_nb.predict_log_proba(cvector_nb.transform([name]))
    return name_logprob


def get_name_prob(name_raw, f_nb, cvector_nb):
    name = word_normalize(name_raw)
    name_prob = f_nb.predict_proba(cvector_nb.transform([name]))[0]
    return name_prob[1]
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

